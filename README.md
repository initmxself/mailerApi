# mailerApi
Служба уведомлений на основе Django

## Процесс установки

1. клонировать этот репо: 

   `git clone https://gitlab.com/initmxself/mailerApi.git`

2. создать виртуальную среду

   `python -m venv <envnamegoeshere>`

3. активировать виртуальную среду

   `source <envnamegoeshere>/bin/activate`

4. установить все необходимые зависимости

   `pip install -r requirements.txt`
   
5. перейдите к загруженному репозиторию
 
   `cd mailerApi`

6. выполнить миграцию

   `python manage.py makemigrations && python manage.py migrate`
